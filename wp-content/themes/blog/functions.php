<?php


add_action('after_setup_theme', 'theme_register_nav_menu');
function theme_register_nav_menu()
{
    register_nav_menus([
        'header_menu' => 'Меню в шапке',
        'footer_menu' => 'Меню в подвале'
    ]);
}

wp_register_style('my_style', get_template_directory_uri() . '/style.css',
    array(), '1.0', 'screen');

wp_enqueue_style('my_style');


function add_class_to_all_menu_anchors($atts)
{
    $atts['class'] = 'nav-link';

    return $atts;
}

add_filter('nav_menu_link_attributes', 'add_class_to_all_menu_anchors', 10);


if (function_exists('add_theme_support')) {

    add_theme_support('post-thumbnails');

}

add_filter('excerpt_length', function () {
    return 20;
});

function wpschool_create_posttype() {
    register_post_type( 'custom_post',
        array(
            'labels' => array(
                'name' => __( 'Custom Posts' ),
                'singular_name' => __( 'Custom Posts' ),
                'menu_name' => __( 'Custom Posts', 'root' ),
                'all_items' => __( 'All Posts', 'root' ),
                'view_item' => __( 'View Post', 'root' ),
                'add_new_item' => __( 'Add new Post', 'root' ),
                'add_new' => __( 'Add Post', 'root' ),
                'edit_item' => __( 'Edit Post', 'root' ),
                'update_item' => __( 'Update Post', 'root' ),
                'search_items' => __( 'Search Post', 'root' ),
                'not_found' => __( 'Post not found', 'root' ),
                'not_found_in_trash' => __( 'Post not found in trash', 'root' ),
            ),
            'public' => true,
            'has_archive' => true,
            'description' => __( 'Post directory', 'root' ),
            'rewrite' => array('slug' => 'custom-post'),
            'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
            'taxonomies' => array( 'custom_taxonomy'),
        )
    );
}
add_action( 'init', 'wpschool_create_posttype' );


add_action( 'init', 'create_taxonomy' );
function create_taxonomy(){

    // список параметров: wp-kama.ru/function/get_taxonomy_labels
    register_taxonomy( 'custom_taxonomy', [ 'custom_post' ], [
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => [
            'name'              => 'Name of post',
            'singular_name'     => 'Name of post',
            'search_items'      => 'Search Genres',
            'all_items'         => 'All Genres',
            'view_item '        => 'View Genre',
            'parent_item'       => 'Parent Genre',
            'parent_item_colon' => 'Parent Genre:',
            'edit_item'         => 'Edit Genre',
            'update_item'       => 'Update Genre',
            'add_new_item'      => 'Add New Genre',
            'new_item_name'     => 'New Genre Name',
            'menu_name'         => 'Name of post',
        ],
        'description'           => '', // описание таксономии
        'public'                => true,
        'hierarchical'          => false,
        'rewrite'               => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null,
        'show_admin_column'     => false,
        'show_in_rest'          => null, // добавить в REST API
        'rest_base'             => null, // $taxonomy
        // '_builtin'              => false,
        //'update_count_callback' => '_update_post_term_count',
    ] );
}
