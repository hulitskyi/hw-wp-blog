<!--<div class="sidebar-box">
    <h3 class="heading">Categories</h3>
    <ul class="categories">
        <?php
/*        $args = array(
            'public' => true,
            '_builtin' => false
        );
        $output = 'names'; // names or objects, note names is the default
        $operator = 'and';   // 'and' or 'or'
        $post_types = get_post_types($args, $output, $operator);
        */?>

        <?php /*foreach ($post_types as $post_type) : */?>
            <li>
                <a href="<?php /*echo get_post_type_archive_link($post_type); */?>"><?php /*echo ucfirst($post_type) */?>
                    <span>(<?php /*echo wp_count_posts($post_type)->publish; */?>)</span></a>
            </li>

        <?php /*endforeach; */?>

        <?php
/*        $args = array(
            '_builtin' => false
        );
        $taksonomii = get_taxonomies($args, 'objects');
        */?>
        <?php /*foreach ($taksonomii as $takso) : */?>

            <?php /*$terms = get_terms($takso->name); */?>

            <?php /*foreach ($terms as $term) : */?>

                <li>
                    <a href="<?php /*echo get_term_link($term->slug, $takso->name); */?>">
                        <?php /*echo $term->name; */?>
                        <span><?php /*echo $term->count; */?></span>
                    </a>
                </li>

            <?php /*endforeach; */?>

        <?php /*endforeach; */?>
    </ul>
</div>-->
<div class="single-sidebar-widget post-category-widget">
    <h4 class="single-sidebar-widget__title">Category</h4>
    <ul class="cat-list mt-20">
        <?php
        $args = array(
            'public' => true,
            '_builtin' => false
        );
        $output = 'names'; // names or objects, note names is the default
        $operator = 'and';   // 'and' or 'or'
        $post_types = get_post_types($args, $output, $operator);
        ?>
        <?php foreach ($post_types as $post_type) : ?>
            <li>
                <a href="<?php echo get_post_type_archive_link($post_type); ?>" class="d-flex justify-content-between">
                    <p><?php echo ucfirst($post_type) ?></p>
                    <p>(<?php echo wp_count_posts($post_type)->publish; ?>)</p>
                </a>
            </li>
        <?php endforeach; ?>
        <?php
        $args = array(
            '_builtin' => false
        );
        $taksonomii = get_taxonomies($args, 'objects');
        ?>
        <?php foreach ($taksonomii as $takso) : ?>

            <?php $terms = get_terms($takso->name); ?>

            <?php foreach ($terms as $term) : ?>

                <li>
                    <a href="<?php echo get_term_link($term->slug, $takso->name); ?>" class="d-flex justify-content-between">
                       <p><?php echo $term->name; ?></p>
                        <p>(<?php echo $term->count; ?>)</p>
                    </a>
                </li>

            <?php endforeach; ?>

        <?php endforeach; ?>
    </ul>
</div>


