<?php get_header(); ?>
<?php $post =  get_post(get_the_ID(), ARRAY_A);?>
    <!--================ Hero sm Banner start =================-->
    <section class="mb-30px">
        <div class="container">
            <div class="hero-banner hero-banner--sm">
                <div class="hero-banner__content">
                    <h1>Current Post</h1>
                    <nav aria-label="breadcrumb" class="banner-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo get_home_url() ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Post</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!--================ Hero sm Banner end =================-->

    <section class="blog-post-area section-margin">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="main_blog_details">
                        <?php echo get_the_post_thumbnail($post['ID'],'large', array('class'=>'img-fluid'))?>
                        <a href="#"><h4><?php echo $post['post_title']?></h4></a>
                        <div class="user_details">

                            <?php
                            $currentPostTaxo = get_post_taxonomies($post['ID']);
                            $currentPostTerms = wp_get_post_terms($post['ID'], $currentPostTaxo[0], array('fields' => 'names'));
                            ?>
                            <div class="float-left">
                                <?php if ($currentPostTerms) : ?>
                                    <?php for($i = 0; $i < sizeof($currentPostTerms); $i++ ) {?>
                                        <a href="<?php echo get_term_link($currentPostTerms[$i], $currentPostTaxo[0]); ?>">
                                            <?php echo $currentPostTerms[$i]; ?>
                                        </a>
                                    <?php }; ?>
                                <?php endif; ?>
                            </div>
                            <div class="float-right mt-sm-0 mt-3">
                                <div class="media">
                                    <div class="media-body">
                                        <p><?php echo get_the_date() . ' ' . get_the_time();  ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p><?php echo $post['post_content']; ?></p>
                        <div class="news_d_footer flex-column flex-sm-row">
                            <a class="justify-content-sm-center ml-sm-auto mt-sm-0 mt-2"><span class="align-middle mr-2"><i class="ti-themify-favicon"></i></span><?php echo $post['comment_count']?> Comments</a>
                            <div class="news_socail ml-sm-auto mt-sm-0 mt-2">
                                <?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Start Blog Post Siddebar -->


            <!-- Start Blog Post Siddebar -->
            <div class="col-lg-4 sidebar-widgets">
                <div class="widget-wrap">


                    <?php get_sidebar(); ?>




                </div>
            </div>
            </div>
        <!-- End Blog Post Siddebar -->


    </section>


<?php get_footer(); ?>