<?php
/*
 * Template name: Category blog
 * Template post type: blog
 * */
?>
<?php get_header(); ?>
    <!--================ Hero sm Banner start =================-->
    <section class="mb-30px">
        <div class="container">
            <div class="hero-banner hero-banner--sm">
                <div class="hero-banner__content">
                    <h1>Category Page</h1>
                    <nav aria-label="breadcrumb" class="banner-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo get_home_url() ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Category Page</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!--================ Hero sm Banner end =================-->

    <!--================ Start Blog Post Area =================-->
    <section class="blog-post-area section-margin">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <?php
                        $args = array('category' => '1');
                        $lastposts = get_posts($args);
                        foreach ($lastposts as $post) {
                            setup_postdata($post);
                            ?>
                            <div class="col-md-6">
                                <div class="single-recent-blog-post card-view">
                                    <div class="thumb">
                                        <img class="card-img rounded-0"
                                             src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" alt="">
                                        <ul class="thumb-info">
                                            <li><a href="<?php the_permalink(); ?>"><i class="ti-notepad"></i><?php echo get_the_date(); ?></a></li>
                                            <li><a href="<?php the_permalink(); ?>">
                                                    <i class="ti-themify-favicon"></i><?php echo wp_count_comments($post->ID)->total_comments; ?> Comments
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="details mt-20">
                                        <a href="<?php the_permalink(); ?>">
                                            <h3><?php the_title() ?></h3>
                                        </a>

                                        <?php
                                        the_excerpt(); ?>
                                        <a class="button" href="<?php the_permalink(); ?>">Read More <i
                                                    class="ti-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <div class="col-lg-4 sidebar-widgets">
                    <div class="widget-wrap">


                        <?php get_sidebar(); ?>




                    </div
                <!-- End Blog Post Siddebar -->
            </div>
    </section>
    <!--================ End Blog Post Area =================-->

<?php get_footer(); ?>