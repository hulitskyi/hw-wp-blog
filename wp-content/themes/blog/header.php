<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sensive Blog - Home</title>
    <link rel="icon" href="img/Fevicon.png" type="image/png">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/vendors/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/vendors/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/vendors/linericon/style.css">
    <link rel="stylesheet"
          href="<?php echo get_template_directory_uri() ?>/vendors/owl-carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/vendors/owl-carousel/owl.carousel.min.css">

    <link rel="stylesheet" href="<?php echo get_stylesheet_uri() ?>">
    <?php wp_head(); ?>
</head>
<body>

<!--================Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container box_1620">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="<?php echo get_home_url() ?>"><img
                            src="<?php echo get_template_directory_uri() ?>/img/logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <?php
                wp_nav_menu(array(
                    'menu' => 'Header',
                    'container' => 'div',
                    'container_class' => 'collapse navbar-collapse offset',
                    'container_id' => 'navbarSupportedContent',
                    'menu_class' => 'nav navbar-nav menu_nav justify-content-center',
                    'menu_id' => ''
                ));
                ?>

            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->

