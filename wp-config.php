<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-blog' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'umz-7%c,B>0_o%O?GkP~E;Rs 3#WsXU=)QjE{rOB(&>-! C~{ fIj@?=ztu. T+)' );
define( 'SECURE_AUTH_KEY',  'iANQR=1;8hZef621%ZhP!UDWx4qS5w4cl?8b|DXo_zfAht~~GI_9s:k(x$M68<`r' );
define( 'LOGGED_IN_KEY',    'BuX{21Dn.N:gdOS0ZxWx7Tw1)liNpbpfB.WXK`^,p]3yEBav][bMG:? 5|49n_6p' );
define( 'NONCE_KEY',        'D.mG@vOsaH;.6yz.4b]i1R]7*.[&<d-GL$Uuu0*?4/Fhz*SK3n@:@wn4@B 6S}Yt' );
define( 'AUTH_SALT',        'O3:k@ThYsge(C-hT1swlXN8!lj$}V8%18JJ:D~7,l@(__Poi05}42r#j[**3_%k(' );
define( 'SECURE_AUTH_SALT', 'QAjkpTUGWzFKynFqQ)92Lo#Dp&~XE/xaoeCDZef&7ay(pObd8_WpMY(l.ML}FLqB' );
define( 'LOGGED_IN_SALT',   'H<&Ei;#3n[E=MR/_Mz[JD`o.ja2PkjRE1;??{}k,NsXDUzeC6]4xa/Y>/o:&Gwzf' );
define( 'NONCE_SALT',       'W_~=8,nR<1 &hwHw|1TUQ6VPo1/ Yd]B?;0uO.Y#=fRX;%(.hW5*QIkZ^~PNq>>=' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
